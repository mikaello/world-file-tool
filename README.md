# World file tool

World file Calculator: https://web.archive.org/web/20160304051310/http://egb13.net/2009/03/worldfile-calculator//

Calculator origin: https://freegeographytools.com/2009/online-worldfile-calculator


Worldfile-basics:
https://freegeographytools.com/2007/worldfile-basics
https://en.wikipedia.org/wiki/World_file


Tools:
Java - https://github.com/zer0infinity/WorldFileTool
Go - https://github.com/xeonx/worldfile/blob/master/worldfile.go
Node - https://github.com/suarasaur/node-worldfile
