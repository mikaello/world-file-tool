/*
 * $Revision: 1.1 $
 * $State: Exp $
 *
 * ----------------------------------------------------------------------------
 * From http://egb13.net/
 *
 * Copyright (C) 2011 by EGB13.net
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ----------------------------------------------------------------------------
 *
 * ----------------------------------------------------------------------------
 * removes (makes transparent) certain colors in a transparent GIF or PNG file
 * used to remove noise from NOAA weather radar images, tho at the loss of some
 * valid low-intensity returns.  Intended for use on the radar-only images from
 * http://radar.weather.gov/Conus/RadarImg/
 *
 * lately added "-c" option to specify a single color to remove, used for removing
 * the black background on images found at http://mesonet.agron.iastate.edu/docs/nexrad_composites/
 *
 *- 2011-09-01: Switch to Image::Magick package as that's what works with
 *- ActiveState Perl for this utility.
 * ----------------------------------------------------------------------------
 */
function CalcWorldFile() {

	var lat1 =  +document.getElementById('lat1').value ;
	if (document.getElementById("lat1neg").selected == true)
		lat1 = 0 - lat1 ;

	var lon1 =  +document.getElementById('lon1').value ;
	if (document.getElementById("lon1neg").selected == true)
		lon1 = 0 - lon1 ;

	var lat2 =  +document.getElementById('lat2').value ;
	if (document.getElementById("lat2neg").selected == true)
		lat2 = 0 - lat2 ;

	var lon2 =  +document.getElementById('lon2').value ;
	if (document.getElementById("lon2neg").selected == true)
		lon2 = 0 - lon2 ;

	var xsize = +document.getElementById('xsize').value ;
	var ysize = +document.getElementById('ysize').value ;

	if (lon1 < lon2) {
		var t = +lon1 ;
		lon1 = lon2 ;
		lon2 = t ;
	}
	var ppx = (lon1 - lon2) / xsize ;

	if (lat1 > lat2) {
		var t = +lat1 ;
		lat1 = lat2 ;
		lat2 = t ;
	}
	var ppy = (lat1 - lat2) / ysize ;

	lon2 += (ppx / 2) ; // x center of pixel
	lat2 += (ppy / 2) ; // y center of pixel

	var wf = ppx.toString() + "\n" +
		"0.00000\n0.00000\n" +
		ppy.toString() + "\n" +
		lon2.toString() + "\n" +
		lat2.toString() ;

	document.getElementById('wf').value = wf ;
} 


/*
     FILE ARCHIVED ON 01:51:49 Mar 09, 2016 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 17:54:54 Aug 20, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 875.646 (3)
  esindex: 0.015
  captures_list: 1176.799
  CDXLines.iter: 35.287 (3)
  PetaboxLoader3.datanode: 652.167 (4)
  exclusion.robots: 0.842
  exclusion.robots.policy: 0.815
  RedisCDXSource: 257.04
  PetaboxLoader3.resolve: 537.688 (2)
  load_resource: 415.445
*/